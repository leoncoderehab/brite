<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

require_once('metabox/featured-video/metabox.php');

if (function_exists('add_theme_support')) {
	add_theme_support('menus');
}
$content_width = 450;
add_theme_support('post-thumbnails');
add_image_size('blog-img', 217, 161, true);
add_image_size('front-blog-img', 151, 122, true);
add_image_size('front-logo-img', 170, 90, true);
add_image_size('ref-logo-img', 155, 88, true);
add_image_size('releted-img', 138, 101, true);
automatic_feed_links();

if ( function_exists('register_sidebar') ) {
	register_sidebar(array('name' =>'Header SideBar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
	register_sidebar(array('name' =>'HomePage SideBar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="widgettitle">',
		'after_title' => '</h2>',
	));
	register_sidebar(array('name' =>'HomePage Right SideBar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div><div class="clear"></div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
	register_sidebar(array('name' =>'Right SideBar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
	register_sidebar(array('name' =>'Footer SideBar',
		'before_widget' => '<div id="%1$s" class="list widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));
}

function register_my_menus() {
	register_nav_menus(
		array(
	'Topmenu' => __('Topmenu Navigation', 'twentyten'),
	'Footermenu' => __('Footermenu Navigation', 'twentyten'),
	'Slidermenu' => __('Slidermenu Navigation', 'twentyten'),
	)
	);
}
add_action( 'init', 'register_my_menus' );

function content($limit) {
  $content = explode(' ', get_the_content(), $limit);
  if (count($content)>=$limit) {
    array_pop($content);
    $content = implode(" ",$content).'...';
  } else {
    $content = implode(" ",$content);
  }
  $content = preg_replace('/\[.+\]/','', $content);
  $content = apply_filters('the_content', $content);
  $content = str_replace(']]>', ']]&gt;', $content);
  return $content;
}
//slider custom post type
add_action('init', 'create_slider');
function create_slider() {

	register_taxonomy('tax_slider',
		'slider',
		array("hierarchical" => true,
			"label" => "Slider categorieen",
			"singular_label" => "Sliders",
			'update_count_callback' => '_update_post_term_count',
			'query_var' => true,
			'rewrite' => array( 'slug' => 'service', 'with_front' => false ),
			'public' => true,'show_ui' => true,
			'show_tagcloud' => true,
			'_builtin' => false,
			'show_in_nav_menus' => true
		)
	);

	$slider_args = array(
		'labels' => array(
			'name' => __( 'Slider' ),
			'singular_name' => __( 'Slide' ),
			'add_new' => __('Add Slide'),
			'all_items' => __('All Slides'),
			'edit_item' => __('Edit Slide'),
			'add_new_item' => __('Add Slide'),
		),
		'public' => true,
		'rewrite' => array('slug' => 'front-slider'),
		'menu_icon' => get_stylesheet_directory_uri() . '/images/slider-icon.png',
		'has_archive' => true,
		'supports' => array('title','thumbnail','editor'),
		'hierarchical' => true,
		'taxonomies' => array('tax_slider'),
	);
	register_post_type('slider', $slider_args);
	flush_rewrite_rules();

}
//referenties custom post type
add_action('init', 'create_referenties');
function create_referenties() {
	$referenties_args = array(
		'labels' => array(
			'name' => __( 'Referenties' ),
			'singular_name' => __( 'Referentie' ),
			'add_new' => __('Add Referentie'),
			'all_items' => __('All Referentie'),
			'edit_item' => __('Edit Referentie'),
			'add_new_item' => __('Add Referentie'),
		),
		'public' => true,
		'rewrite' => array('slug' => 'front-referenties'),

		'hierarchical' => false,
		'has_archive' => true,
		'supports' => array('title','thumbnail','editor')
	);
	register_post_type('referenties', $referenties_args);
	flush_rewrite_rules();
}


//Producten custom post type
add_action('init', 'create_Producten');
function create_Producten() {
	$Producten_args = array(
		'labels' => array(
			'name' => __( 'Producten' ),
			'singular_name' => __( 'Producten' ),
			'add_new' => __('Add Producten'),
			'all_items' => __('All Producten'),
			'edit_item' => __('Edit Producten'),
			'add_new_item' => __('Add Producten'),
		),
		'public' => true,
		'rewrite' => array('slug' => 'front-Producten'),

		'hierarchical' => false,
		'has_archive' => true,
		'supports' => array('title','thumbnail','editor')
	);
	register_post_type('Producten', $Producten_args);
	flush_rewrite_rules();
}

/**
 * producten Widget Class
 */
class producten_widget extends WP_Widget {


    /** constructor -- name this the same as the class above */
    function producten_widget() {
        parent::WP_Widget(false, $name = 'Producten Widget');
    }

    /** @see WP_Widget::widget -- do not rename this */
    function widget($args, $instance) {
        extract( $args );
        $title 		= apply_filters('widget_title', $instance['title']);
        $message1 	= $instance['message'];
        ?>
              <?php echo $before_widget; ?>
                  <?php if ( $title )
                        echo $before_title . $title . $after_title;?>


							<ul>

                            	<?php

								 $vac = array( 'post_type' => 'producten','showposts' => $message1);
					 $vac_posts = new WP_Query ($vac);

					 if ($vac_posts->have_posts()) : while ($vac_posts->have_posts()) : $vac_posts->the_post();
								 ?>

                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>


                 <?php  endwhile; endif; wp_reset_query(); ?>
                 </ul>
              <?php echo $after_widget; ?>
        <?php
    }

    /** @see WP_Widget::update -- do not rename this */
    function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['message'] = strip_tags($new_instance['message']);
        return $instance;
    }

    /** @see WP_Widget::form -- do not rename this */
    function form($instance) {

        $title 		= esc_attr($instance['title']);
        $message	= esc_attr($instance['message']);
        ?>
         <p>
          <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
          <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
		<p>
          <label for="<?php echo $this->get_field_id('message'); ?>"><?php _e('Show producten'); ?></label>
          <input class="widefat" id="<?php echo $this->get_field_id('message'); ?>" name="<?php echo $this->get_field_name('message'); ?>" type="text" value="<?php echo $message; ?>" />
        </p>
        <?php
    }


} // end class producten_widget
add_action('widgets_init', create_function('', 'return register_widget("producten_widget");'));
/**
 * Referentie Widget Class
 */
class referenties_widget extends WP_Widget {


    /** constructor -- name this the same as the class above */
    function referenties_widget() {
        parent::WP_Widget(false, $name = 'Referentie Widget');
    }

    /** @see WP_Widget::widget -- do not rename this */
    function widget($args, $instance) {
        extract( $args );
        $title 		= apply_filters('widget_title', $instance['title']);
        $message 	= $instance['message'];
		 $link 	= $instance['link'];
        ?>
              <?php echo $before_widget; ?>
                  <?php if ( $title )
                        echo $before_title . $title . $after_title;?>
                        <div class="customer">
                        <div class="slider">
                        <div class="flexslider-sidebar">
                <ul class="slides">

                 <?php
				     $i=1;
				     $ref = array( 'post_type' => 'referenties','showposts' => -1 ,'featured' => 'yes');
					 $ref_posts = new WP_Query ($ref);

					 if ($ref_posts->have_posts()) : while ($ref_posts->have_posts()) : $ref_posts->the_post();
				 ?>

               <li class="nolist">
                       <?php the_field('slider_sidebar_text'); ?>

                        <div class="info">

                        <span><?php the_title(); ?> <span><?php the_field('designation'); ?></span></span>

                        <div class="photo">

                        <img src="<?php the_field('photo'); ?>" alt="" />

                        </div>

                        </div>

                        <div class="clear"></div>

                        <a href="<?php the_permalink() ?>" class="btn"><?php echo $message; ?></a>
               </li>
            <?php $i++;  endwhile; endif; wp_reset_query(); ?>
            </ul>
            </div>
            </div>
            </div>
              <?php echo $after_widget; ?>
        <?php
    }

    /** @see WP_Widget::update -- do not rename this */
    function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['message'] = strip_tags($new_instance['message']);
		$instance['link'] = strip_tags($new_instance['link']);
        return $instance;
    }

    /** @see WP_Widget::form -- do not rename this */
    function form($instance) {

        $title 		= esc_attr($instance['title']);
        $message	= esc_attr($instance['message']);
		 $link	= esc_attr($instance['link']);
        ?>
         <p>
          <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
          <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
        <p>
          <label for="<?php echo $this->get_field_id('link'); ?>"><?php _e('Referenties Link'); ?></label>
          <input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo $link; ?>" />
        </p>
         <p>
          <label for="<?php echo $this->get_field_id('message'); ?>"><?php _e('Referenties Text'); ?></label>
          <input class="widefat" id="<?php echo $this->get_field_id('message'); ?>" name="<?php echo $this->get_field_name('message'); ?>" type="text" value="<?php echo $message; ?>" />
        </p>


        <?php
    }


} // end class referenties_widget
add_action('widgets_init', create_function('', 'return register_widget("referenties_widget");'));


//------ our setyting--------------
$themename = "Brite";
$shortname = str_replace(' ', '_', strtolower($themename));

function get_theme_option($option)
{
	global $shortname;
	return stripslashes(get_option($shortname . '_' . $option));
}

function get_theme_settings($option)
{
	return stripslashes(get_option($option));
}

$options = array (

	array(	"type" => "open"),
	// Start copyright
		array(	"name" => "Contact Text",
		"desc" => "Enter Contact Text here",
		"id" => $shortname."_contact",
		"std" => "#",
		"type" => "textarea"),
		// End copyright

		// Start Facebook Link
		array(	"name" => "Facebook Link",
		"desc" => "Enter Facebook Link here",
		"id" => $shortname."_facebook",
		"std" => "#",
		"type" => "text"),
		// End Facebook Link

		// Start Twitter Link
		array(	"name" => "Twitter Link",
		"desc" => "Enter Twitter Link here",
		"id" => $shortname."_twitter",
		"std" => "#",
		"type" => "text"),
		// End Facebook Link

		// Start Google Plus Link
		array(	"name" => "Google Plus Link",
		"desc" => "Enter Google Plus here",
		"id" => $shortname."_google",
		"std" => "#",
		"type" => "text"),
		// End Facebook Link

		// Start Linkedin Link
		array(	"name" => "Linkedin Link",
		"desc" => "Enter Linkedin Link here",
		"id" => $shortname."_Linkedin",
		"std" => "#",
		"type" => "text"),
		// End Linkedin Link



		// Start copyright
		array(	"name" => "Copyright Text",
		"desc" => "Enter Copyright Text here",
		"id" => $shortname."_copyright",
		"std" => "#",
		"type" => "textarea"),
		// End copyright


	array(	"type" => "close")

);

function mytheme_add_admin() {
    global $themename, $shortname, $options;

    if ( $_GET['page'] == basename(__FILE__) ) {

        if ( 'save' == $_REQUEST['action'] ) {

                foreach ($options as $value) {
                    update_option( $value['id'], $_REQUEST[ $value['id'] ] ); }

                foreach ($options as $value) {
                    if( isset( $_REQUEST[ $value['id'] ] ) ) { update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); } else { delete_option( $value['id'] ); } }

                echo '<meta http-equiv="refresh" content="0;url=themes.php?page=functions.php&saved=true">';
                die;

        }
    }

    add_theme_page($themename." Options", "".$themename." Options", 'edit_themes', basename(__FILE__), 'mytheme_admin');
}


function mytheme_admin_init() {

    global $themename, $shortname, $options;

    $get_theme_options = get_option($shortname . '_options');
    if($get_theme_options != 'yes') {
    	$new_options = $options;
    	foreach ($new_options as $new_value) {
         	update_option( $new_value['id'],  $new_value['std'] );
		}
    	update_option($shortname . '_options', 'yes');
    }
}

function mytheme_admin() {

    global $themename, $shortname, $options;

    if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings saved.</strong></p></div>';

?>
<div class="wrap">
<h2><?php echo $themename; ?> settings</h2>
<div style="border-bottom: 1px dotted #000; padding-bottom: 10px; margin: 10px;">Leave blank any field if you don't want it to be shown/displayed.</div>
<form method="post">



<?php foreach ($options as $value) {

	switch ( $value['type'] ) {

		case "open":
		?>
        <table width="100%" border="0" style=" padding:10px;">



		<?php break;

		case "close":
		?>

        </table><br />


		<?php break;

		case "title":
		?>
		<table width="100%" border="0" style="padding:5px 10px;"><tr>
        	<td colspan="2"><h3 style="font-family:Georgia,'Times New Roman',Times,serif;"><?php echo $value['name']; ?></h3></td>
        </tr>


		<?php break;

		case 'text':
		?>

        <tr>
            <td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
            <td width="80%"><input style="width:100%;" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php echo get_theme_settings( $value['id'] ); ?>" /></td>
        </tr>

        <tr>
            <td><small><?php echo $value['desc']; ?></small></td>
        </tr><tr><td colspan="2" style="margin-bottom:5px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>

		<?php
		break;

		case 'textarea':
		?>

        <tr>
            <td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
            <td width="80%"><textarea name="<?php echo $value['id']; ?>" style="width:100%; height:140px;" type="<?php echo $value['type']; ?>" cols="" rows=""><?php echo get_theme_settings( $value['id'] ); ?></textarea></td>

        </tr>

        <tr>
            <td><small><?php echo $value['desc']; ?></small></td>
        </tr><tr><td colspan="2" style="margin-bottom:5px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>

		<?php
		break;

		case 'select':
		?>
        <tr>
            <td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
            <td width="80%">
				<select style="width:240px;" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>">
					<?php
						foreach ($value['options'] as $option) { ?>
						<option value="<?php echo $option['value']; ?>" <?php if ( get_theme_settings( $value['id'] ) == $option['value']) { echo ' selected="selected"'; } ?>><?php echo $option['title']; ?></option>
						<?php } ?>
				</select>
			</td>
       </tr>

       <tr>
            <td><small><?php echo $value['desc']; ?></small></td>
       </tr><tr><td colspan="2" style="margin-bottom:5px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>

		<?php
        break;

		case "checkbox":
		?>
            <tr>
            <td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
                <td width="80%"><? if(get_theme_settings($value['id'])){ $checked = "checked=\"checked\""; }else{ $checked = ""; } ?>
                        <input type="checkbox" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />
                        </td>
            </tr>

            <tr>
                <td><small><?php echo $value['desc']; ?></small></td>
           </tr><tr><td colspan="2" style="margin-bottom:5px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>

        <?php 		break;
//----------------------------------upload---------------------
			case 'upload':
		?>
        <tr>
            <td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
            <td width="80%">
			<input type="file" name="<?php echo $option['title']; ?>"  id="<?php echo $value['id']; ?>"  />

			</td>
       </tr>

       <tr>
            <td><small><?php echo $value['desc']; ?></small></td>
       </tr><tr><td colspan="2" style="margin-bottom:5px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>

		<?php
        break;


}
}
?>

<!--</table>-->


<p class="submit">
<input name="save" type="submit" value="Save changes" />
<input type="hidden" name="action" value="save" />
</p>
</form>

<?php }
add_action('admin_menu', 'mytheme_add_admin');
?>
