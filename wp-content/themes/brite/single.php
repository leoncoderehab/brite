<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header();
?>
<!--  / left container \ -->
                <div id="leftCntr">

                	<!--  / path box \ -->
                    <div class="pathBox ">

				<?php if ( function_exists('yoast_breadcrumb') ) {
                yoast_breadcrumb('<div id="breadcrumbs">','</div>');
                } ?>

                    </div>
	                <!--  \ path box / -->



                    <div class="clear"></div>


                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                     <!--  / step box \ -->
                    <div class="stepBox">

                    <h1><?php the_title(); ?></h1>

                            <div class="photo new">

                            	<?php the_post_thumbnail('blog-img'); ?>

                            </div>

                    <?php the_content(); ?>

                          </div>
	                <!--  \ step box / -->


                    <div class="clear"></div>

                    <!--  / view box \ -->
                    <div class="viewBox">

                    	<h4>Gerelateerde artikelen:</h4>
                        <ul>
                        <?php

$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'post__not_in' => array($post->ID),'posts_per_page' => 6 ) );

if( $related ) $i=1; foreach( $related as $post ) {
setup_postdata($post); ?>
 <li><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('releted-img'); ?><span><?php the_title(); ?></span></a></li>

   <?php if($i%3==0) { ?><div class="clear"></div><?php  } ?>
<?php  $i++;}
wp_reset_postdata(); ?>
 </ul>

                    </div>
	                <!--  \ view box / -->

                    <div class="clear"></div>

                    <!--  / follow box \ -->
                    <div class="followBox">

                        <ul>
                        	<li><div class="g-plusone"></div></li>
                           <li class="like"> <span class='st_facebook_hcount' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>' displayText='share'></span></li>
                            <li class="tweet"><span class='st_twitter_hcount' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>' displayText='share'></li>
                            <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http://www.yoururl.com&title=Your title&summary=Your summary text&source=Your source" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/follow_img4.png" alt="" /></a></li>
                            <li><a href="//www.pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.flickr.com%2Fphotos%2Fkentbrew%2F6851755809%2F&media=http%3A%2F%2Ffarm8.staticflickr.com%2F7027%2F6851755809_df5b2051c9_z.jpg&description=Next%20stop%3A%20Pinterest" data-pin-do="buttonPin" data-pin-config="beside"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" /></a></li>
                        </ul>

                    </div>
	                <!--  \ follow box / -->

                    <div class="clear"></div>

                     	<div class="authorBox about">

                            <?php echo get_avatar( get_the_author_id() , 106 ); ?>

                            <div class="content">

                                <p><?php the_author_description(); ?><a href="<?php the_permalink(); ?>">Meer artikelen van <?php the_author_firstname(); ?> ></a></p>

                            </div>

                        </div>

                        <div class="clear"></div>


                         <!--  / reply box \ -->
                    <div class="replyBox">


                         <?php comments_template(); ?>


                    </div>
	                <!--  \ reply box / -->



    <?php endwhile; else: ?>

    <p>Sorry, no posts matched your criteria.</p>

    <?php endif; ?>







                </div>
				<!--  \ left container / -->


                <!--  / right container \ -->
                <div id="rightCntr">

   <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Right SideBar') ) : ?> <?php endif; ?>

                </div>
                <!--  \ right container / -->

<?php get_footer(); ?>
