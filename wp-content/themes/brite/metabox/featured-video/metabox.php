<?php

add_action( 'add_meta_boxes', 'core_featured_video_metabox_add' );
add_action( 'save_post', 'core_featured_video_metabox_save' );

function core_featured_video_metabox_add()
{
	add_meta_box( 'date_select', 'Featured video', 'core_featured_video_metabox', 'page', 'normal', 'high' );
}

function core_featured_video_metabox( $post )
{
	$values = get_post_custom( $post->ID );
	$video_url = isset( $values['featured_video_url'] ) ? esc_attr( $values['featured_video_url'][0] ) : "";
	$video_open_on_page_load = isset( $values['featured_video_on_pageload'] ) ? esc_attr( $values['featured_video_on_pageload'][0] ) : "";


	include 'view.php';
}

function core_featured_video_metabox_save( $post_id )
{

	// Defines
	$video_url = $_POST["featured_video_url"];
	$video_open_on_page_load = $_POST["featured_video_on_pageload"];

	//die($video_open_on_page_load);

	// Bail if we're doing an auto save
	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
	// if our current user can't edit this post, bail
	if( !current_user_can( 'edit_post' ) ) return;
	//var_dump( $_POST ); die;

	if( isset( $video_url ) ) {
		update_post_meta( $post_id, 'featured_video_url', $video_url);
	}
	update_post_meta( $post_id, 'featured_video_on_pageload', $video_open_on_page_load);
}

?>
