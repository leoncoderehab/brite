<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header(); ?>

<!--  / left container \ -->
                <div id="leftCntr">

                	<!--  / path box \ -->
                    <div class="pathBox ">

                					<?php if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('<div id="breadcrumbs">','</div>');
} ?>


                    </div>
	                <!--  \ path box / -->

                    <div class="clear"></div>

                    <!--  / wire box \ -->
                    <div class="wireBox">

<?php if (have_posts()) : ?>

    <h2 class="pagetitle">Search Results</h2>
    <?php while (have_posts()) : the_post(); ?>
                        <div class="blog">

                        	<h1><?php the_title(); ?></h1>

                            <?php if(has_post_thumbnail()){ ?>
                            <div class="photo">

                            	<?php the_post_thumbnail('blog-img'); ?>

                            </div>
                            <?php } ?>

                            <div class="content <?php if(!has_post_thumbnail()){ echo "fullpost" ; }?>">

                             <?php  the_excerpt(); ?> <a class="meer" href="<?php the_permalink(); ?>">verder lezen ></a>


                            </div>

                            <div class="clear"></div>

                            <div class="info">

                                <ul>
                                	<li class="time"><?php the_time('l j F Y') ?></li>
                                    <li><a href="#"><?php the_author() ?></a></li>
                                    <li><a href="#"><?php comments_popup_link('0 Reacties', '1 Reacties', '% Reacties'); ?></a></li>
                                   <li class="last">

<?php $i=1; foreach((get_the_category()) as $category) {

	$category_link = get_category_link( $category->cat_ID );
	?>
   <a href="<?php echo $category_link; ?>"><?php if($i==1) { echo '  ' ; } else { echo '  , ' ; }  ?><?php echo $category->cat_name ; ?></a>
   <?php $i++; }?>
    </li>
                                </ul>

                                <a href="<?php the_permalink(); ?>" class="btn">1</a>

                            </div>

                        </div>
       <?php endwhile; ?>
     <div class="clear"></div>
   <?php if(function_exists('wp_paginate')) {
    wp_paginate();
} ?>

    <?php else : ?>

    <h2 class="center">Not Found</h2>

    <p class="center">Sorry, but you are looking for something that isn't here.</p>

	<?php get_search_form(); ?>

    <?php endif; ?>




                    </div>
	                <!--  \ wire box / -->

                </div>
				<!--  \ left container / -->

                <!--  / right container \ -->
                <div id="rightCntr">

   <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Right SideBar') ) : ?> <?php endif; ?>

                </div>
                <!--  \ right container / -->

<?php get_footer(); ?>
