<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

	<head profile="http://gmpg.org/xfn/11">
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

		<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/fonts/fonts.css" type="text/css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/skin.css" />

		<!-- Mobile Specific Metas ================================================== -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


		<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

		<?php wp_head(); ?>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

		<script src="<?php bloginfo('template_url'); ?>/js/respond.src.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/slidernav.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.jcarousel.min.js"></script>
		<script type="text/javascript">

			function mycarousel_initCallback(carousel)
			{
				// Disable autoscrolling if the user clicks the prev or next button.
				carousel.buttonNext.bind('click', function() {
					carousel.startAuto(0);
				});

				carousel.buttonPrev.bind('click', function() {
					carousel.startAuto(0);
				});

				// Pause autoscrolling if the user moves with the cursor over the clip.
				carousel.clip.hover(function() {
					carousel.stopAuto();
				}, function() {
					carousel.startAuto();
				});
			};

			jQuery(document).ready(function() {
				jQuery('#mycarousel').jcarousel({
					auto: 2,
					wrap: 'last',
					initCallback: mycarousel_initCallback
				});
			});

		</script>


		<script src="<?php bloginfo('template_url'); ?>/js/jquery.flexslider.js"></script>
		<script type="text/javascript">
			var s = jQuery.noConflict();
			s(window).load(function() {
				s('.flexslider-home').flexslider({
					animation: "fade",
					controlNav: true,
					directionNav: false,
					start: function(slider){
						s('.flexslider-home-loaded').removeClass('loading');
					}
				});
			});
		</script>
		<script type="text/javascript">
			var s1 = jQuery.noConflict();
			s1(window).load(function() {
				s1('.flexslider-logo').flexslider({
					animation: "slide",
					animationLoop: true,
					itemWidth: 178,
					itemMargin: 0,
					minItems: 2,
					maxItems: 5,
					start: function(slider){
						s1('.flexslider-logo-loaded').removeClass('loading');
					}
				});
			});
		</script>
		<script type="text/javascript">
			var s2 = jQuery.noConflict();
			s2(window).load(function() {
				s2('.flexslider-sidebar').flexslider({
					animation: "slide",
					directionNav: false,
					start: function(slider){
						s2('.flexslider-sidebar-loaded').removeClass('loading');
					}
				});
			});
		</script>
		<!-- Place this tag after the last +1 button tag. -->
		<script type="text/javascript">
			(function() {
				var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
				po.src = 'https://apis.google.com/js/plusone.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			})();
		</script>

		<script src="<?php bloginfo('template_url'); ?>/js/custom-form-elements.js"></script>

	</head>

	<body <?php body_class(); ?>>

		<!--  / wrapper \ -->
		<div id="wrapper">

			<!--  / main container \ -->
			<div id="mainCntr">

				<!--  / header container \ -->
				<div id="headerCntr">

					<!--  / header box \ -->
					<div class="headerBox">



					</div>
					<!--  \ header box / -->

					<!--  / menu box \ -->
					<div class="menuBox">

						<div class="centering">


							<?php  wp_nav_menu( array( 'theme_location' => 'Topmenu' ) ); ?>

						</div>

					</div>
					<!--  \ menu box / -->

				</div>
				<!--  \ header container / -->


