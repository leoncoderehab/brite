<?php

/**

 Template name: Home Page

 */



get_header(); ?>



<!--  / left container \ -->

                <div id="leftCntr">



                	<!--  / path box \ -->

                    <div class="pathBox ">



<?php if ( function_exists('yoast_breadcrumb') ) {

	yoast_breadcrumb('<div id="breadcrumbs">','</div>');

} ?>

                    </div>

	                <!--  \ path box / -->



                    <div class="clear"></div>



                    <!--  / brite box \ -->

                    <div class="briteBox">



					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <?php the_content(); ?>

                    <?php endwhile; endif; ?>



                    </div>

	                <!--  \ brite box / -->



                    <div class="clear"></div>



                    <!--  / site box \ -->

                    <div class="siteBox">



                		 <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('HomePage SideBar') ) : ?> <?php endif; ?>



                    </div>

	                <!--  \ site box / -->



                    <div class="clear"></div>







                </div>

				<!--  \ left container / -->



                <!--  / right container \ -->

                <div id="rightCntr">



                   <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('HomePage Right SideBar') ) : ?> <?php endif; ?>











                </div>

                <!--  \ right container / -->



<?php get_footer(); ?>
