<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>

<div class="clear"></div>
</div>
</div>

<!--  / center container \ -->
<div id="centerCntr">


	<?php

	// if_is_frontpage()
	if(1 == 2) { ?>
	<!--  / slider box \ -->
	<div class="sliderBox">

		<div class="centering">

			<h5>Referenties:</h5>
			<div class="clear"></div>
			<div class="flexslider-logo">
				<div class="flexslider-logo-loaded loading"></div>
				<ul class="slides">
					<?php  query_posts('post_type=referenties&&showposts=90000&orderby=date&order=DESC'); ?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php if(get_field('logo_show_in_homepage_slider')=='1') { ?>
					<li><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('front-logo-img');  ?></a></li>
					<?php } ?>
					<?php  endwhile; endif; wp_reset_query(); ?>
				</ul>
			</div>


		</div>

	</div>
	<!--  \ slider box / -->

	<?php } ?>

</div>
<!--  \ center container / -->
<div class="clear"></div>
</div>
<!--  \ content container / -->

<div class="clear"></div>

<!--  / footer container \ -->
<div id="footerCntr">

	<!--  / footer box \ -->
	<div class="footerBox">

		<div class="centering">


			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer SideBar') ) : ?> <?php endif; ?>


			<div class="list1">

				<?php echo get_theme_option('contact'); ?>

				<ul>
					<?php if(get_theme_option('facebook')){ ?>
					<li><a href="<?php echo get_theme_option('facebook'); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/fb.png" alt="" /></a></li>
					<?php } ?>
					<?php if(get_theme_option('twitter')){ ?>
					<li><a href="<?php echo get_theme_option('twitter'); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/twitter.png" alt="" /></a></li>
					<?php } ?>
					<?php if(get_theme_option('google')){ ?>
					<li><a href="<?php echo get_theme_option('google'); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/gplus.png" alt="" /></a></li>
					<?php } ?>
					<?php if(get_theme_option('Linkedin')){ ?>
					<li><a href="<?php echo get_theme_option('Linkedin'); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/linkedin.png" alt="" /></a></li>
					<?php } ?>
				</ul>

			</div>

		</div>

	</div>
	<!--  \ footer box / -->

	<div class="clear"></div>

	<!--  / copy box \ -->
	<div class="copyBox">

		<div class="centering">

			<?php  wp_nav_menu( array( 'theme_location' => 'Footermenu' ) ); ?>

			<p><?php echo get_theme_option('copyright'); ?></p>

		</div>

	</div>
	<!--  \ copy box / -->

</div>
<!--  \ footer container / -->

</div>
<!--  \ main container / -->

</div>
<!--  \ wrapper / -->

<?php
$video_url  = get_post_meta(get_the_ID(),'featured_video_url')[0];
$show_video_on_page_load  = get_post_meta(get_the_ID(),'featured_video_on_pageload')[0];
echo($show_video_on_page_load);
?>

<section id="CR_videoPopup">
	<div class='background'></div>
	<section class='wrapper'>
		<div class='close'>(x) sluiten</div>
		<iframe src="<?= ($show_video_on_page_load == "on") ? $video_url : '' ?>" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
	</section>
</section>

<script>
	jQuery(function($){
		$cr_vimeopopup = $('#CR_videoPopup')

		$('.vimeopopup').click(function(e) {
			e.preventDefault();
			var target = $(e.currentTarget);
			var src =  target.attr('href');
			if( !src ) src = target.find('a').first().attr('href');

			$cr_vimeopopup.fadeIn(300, function(){
				$cr_vimeopopup.find('iframe').attr('src', src);
			});
		});

		if("<?= $show_video_on_page_load ?>" == "on"){
			$cr_vimeopopup.delay(500).fadeIn(500);
		}

		$cr_vimeopopup.find('.background').click(closePopup);
		$cr_vimeopopup.find('.close').click(closePopup);

		function closePopup(){
			$cr_vimeopopup.find('iframe').attr('src', '');
			$cr_vimeopopup.fadeOut(300);
		}

	})
</script>

<?php wp_footer(); ?>

<script src="<?php bloginfo('template_url'); ?>/js/tinynav.min.js"></script>
<script type="text/javascript">
	var m = jQuery.noConflict();
	m(function () {
		m('#menu-topmenu').tinyNav({
			active: 'selected',
			header: 'Menu'
		});
	});
</script>
</body>
</html>
