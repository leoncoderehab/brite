<?php

/**

 Template name: Homepage Bastards

 */


get_header('bastard'); ?>



<!--  / content container \ -->
<div id="contentCntr">
	<div class="bgcenter"><div class="centering">

		<a href="<?php echo get_option('home'); ?>" class="logo"><img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="" /></a>                    <div class="flexslider-home">
		<div class="flexslider-home-loaded loading"></div>


		<!--  / Slider menu container \ -->
		<div id="slider-menu">
			<?php  wp_nav_menu( array( 'theme_location' => 'Slidermenu' ) ); ?>

		</div>
		<!--  / slider menu container \ -->

		<!-- POPUPS-->
		<div class="slide-popup wifi">
			<button class="close">x</button>
			<img class="stamp" src='<?php echo bloginfo('template_url')?>/images/dokter_stempel.png'>
			<h4>WiFi 3.0  :</h4>

			<p>
				Met Brite WiFi 3.0 haakt Brite in op de groeiende behoefte van ict afdelingen en schoolleiders aan totale ontzorging voor de invulling tussen de school en online uitgevers en online diensten. één loket met één totaalconcept</p>
			<!-- <a href="leesmeer" class="video-btn"><img src="<?php bloginfo('template_url')?>/images/video-btn.png"/>Video bekijken</a> -->
			<a href="<?php bloginfo('wpurl');?>/wifi-3-0-onderwijs-totaalconcept/" class="read-more-btn">Lees meer</a>
		</div>

		<div class="slide-popup ict-scan">
			<button class="close">x</button>
			<img class="stamp" src='<?php echo bloginfo('template_url')?>/images/dokter_stempel.png'>
			<h4>Second opinion :</h4>
			<p>De meeste scholen zijn in de afgelopen jaren gestart met WIFI in de school.  Echter bij explosieve groei van devices en het gebruik ervan ontstaat een trage ELO, grote kans op DDoS aanvallen en zijn er leerlingen die digitaal pesten en online saboteren. Brite kan u als geen ander helpen d.m.v. de second opinion voor uw ICT infra. Op basis van dit onderzoek wordt de hele ICT infra, inclusief het wireless netwerk, de prestaties en de invloed van stoorbronnen nauwkeurig onderzocht en in kaart gebracht. Vanuit dit onderzoek volgt een rapportage met bevindingen die uitvoerig met u wordt besproken. </p><p>&nbsp;</p>
			<a target="_blank" href="https://player.vimeo.com/video/147827392?autoplay=true" class="video-btn vimeopopup"><img src="<?php bloginfo('template_url')?>/images/video-btn.png"/>Video bekijken</a>
		</div>

		<div class="slide-popup scholen">
			<button class="close">x</button>
			<img class="stamp" src='<?php echo bloginfo('template_url')?>/images/dokter_stempel.png'>
			<h4>>220 VO-Scholen :</h4>
			<p>Met meer dan 220 VO-Scholen als klant is Brite de grootste leverancier van ICT infra in het voortgezet onderwijs. Wij gaan voor 100% tevreden klanten en met deze visie blijft ons klantenbestand toenemen.</p>
			<a href="<?php bloginfo('wpurl');?>/referentie-kaart/" class="read-more-btn">Lees meer</a>
		</div>

		<div class="slide-popup eduroam">
			<button class="close">x</button>
			<img class="stamp" src='<?php echo bloginfo('template_url')?>/images/dokter_stempel.png'>
			<h4>Eduroam :</h4>
			<p>Eduroam is een gratis internationale dienst voor onderwijs- en overheidsinstellingen die een wifi-netwerk hebben en dit wifi-netwerk beschikbaar stelt aan geregistreerde gebruikers.  Samen met Kennisnet verzorgt Brite eduroam aansluitingen voor het voortgezet onderwijs.</p>
			<a href="<?php bloginfo('wpurl');?>/wifi-oplossingen/eduroam-voortgezet-onderwijs/" class="read-more-btn">Lees meer</a>
		</div>
		<!-- END POPUP \ -->


		<section id="page-mask">



			<section id="slides-bg">
			</section>

			<ul class="slides">

				<?php

				$args = array(
					'post_type' => 'slider',
					'tax_query' => array(
						array(
							'taxonomy' => 'tax_slider',
							'field'    => 'slug',
							'terms'    => 'home',
						),
					),
				);
				$query = new WP_Query( $args );

				if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
				<li>
					<section class="info">
						<?php the_content() ?>
					</section>

					<section class="img-mask">
						<?php the_post_thumbnail(); ?>
					</section>

				</li>
				<?php  endwhile; endif; wp_reset_postdata();  ?>
			</ul>

		</section>
		</div>
		<div class="slogan">

			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Header SideBar') ) : ?> <?php endif; ?>

		</div>
		<div class="search">

			<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">

				<fieldset>

					<input type="text" value="Zoeken..." onblur="if (this.value == '') {this.value = 'Zoeken...';}" onfocus="if(this.value == 'Zoeken...') {this.value = '';}" name="s" id="s" />

					<input type="submit" id="searchsubmit"  value=" " />

				</fieldset>

			</form>

		</div>

		</div>
		<div class="centering">


<?php get_footer(); ?>
