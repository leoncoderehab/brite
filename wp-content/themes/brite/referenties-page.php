<?php
/**
 Template name: Referenties Page
 */

get_header(); ?>

<!--  / left container \ -->
                <div id="leftCntr">
                <!--  / path box \ -->
                    <div class="pathBox ">

					<?php if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('<div id="breadcrumbs">','</div>');
} ?>


                    </div>
	                <!--  \ path box / -->

                    <div class="clear"></div>


    <!--  / list box \ -->
                    <div class="listBox">

                    <h1><?php the_title(); ?></h1>
                     <?php  query_posts('post_type=referenties&&showposts=90000'); ?>
                      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    	<div class="list">

                            <div class="photo">

                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('ref-logo-img'); ?></a>

                            </div>

                            <div class="content textBox">

                                <?php echo content('20'); ?>

                                <span><?php the_field(extra_text); ?></span>

                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?> <span>- <?php  the_field('designation');?></span></a>

                            </div>

                            <div class="men">

                                <img src="<?php the_field('photo'); ?>" alt="" />

                                <a href="<?php the_permalink(); ?>">Lees meer ></a>

                            </div>

                        </div>


                      <?php  endwhile; endif; wp_reset_query(); ?>
                    </div>
	                <!--  \ list box / -->

                    <div class="clear"></div>

                </div>
				<!--  \ left container / -->

                <!--  / right container \ -->
                <div id="rightCntr">

                   <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('HomePage Right SideBar') ) : ?> <?php endif; ?>





                </div>
                <!--  \ right container / -->

<?php get_footer(); ?>
