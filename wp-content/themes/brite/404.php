<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header();
?>

<!--  / left container \ -->
                <div id="leftCntr">

                	<!--  / path box \ -->
                    <div class="pathBox ">

					<?php if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('<div id="breadcrumbs">','</div>');
} ?>


                    </div>
	                <!--  \ path box / -->

                    <div class="clear"></div>

                    <!--  / wire box \ -->
                    <div class="wireBox pageBox">

                        <div class="blog new">

                        	<h1>Error 404 - Not Found</h1>



                        </div>

                    </div>
	                <!--  \ wire box / -->




                </div>
				<!--  \ left container / -->

                <!--  / right container \ -->
                <div id="rightCntr">

                   <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('HomePage Right SideBar') ) : ?> <?php endif; ?>





                </div>
                <!--  \ right container / -->


<?php get_footer(); ?>
