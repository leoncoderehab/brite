<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header();
?>

<!--  / left container \ -->
                <div id="leftCntr">

                	<!--  / path box \ -->
                    <div class="pathBox ">

                					<?php if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('<div id="breadcrumbs">','</div>');
} ?>


                    </div>
	                <!--  \ path box / -->

                    <div class="clear"></div>

                    <!--  / wire box \ -->
                    <div class="wireBox">

<?php if (have_posts()) : ?>

    <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
    <?php /* If this is a category archive */ if (is_category()) { ?>
    <h2 class="pagetitle"><?php single_cat_title(); ?></h2>
    <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
    <h2 class="pagetitle"><?php single_tag_title(); ?></h2>
    <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
    <h2 class="pagetitle"><?php the_time('F jS, Y'); ?></h2>
    <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
    <h2 class="pagetitle"><?php the_time('F, Y'); ?></h2>
    <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
    <h2 class="pagetitle">Archive for <?php the_time('Y'); ?></h2>
    <?php /* If this is an author archive */ } elseif (is_author()) { ?>
    <h2 class="pagetitle">Author Archive</h2>
    <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
    <h2 class="pagetitle">Blog Archives</h2>
    <?php } ?>
    <?php while (have_posts()) : the_post(); ?>
                        <div class="blog">

                        	<h1><a href="<?php the_permalink(); ?>"/><?php the_title(); ?></a></h1>

                            <?php if(has_post_thumbnail()){ ?>
                            <div class="photo">

                            	<a href="<?php the_permalink(); ?>"/><?php the_post_thumbnail('blog-img'); ?></a>

                            </div>
                            <?php } ?>

                            <div class="content <?php if(!has_post_thumbnail()){ echo "fullpost" ; }?>">

							<?php $chkdata = substr(strip_tags(get_the_content(),"<strong>"),0,350);

								$string = explode(' ', $chkdata);
								$cnt = count($string);
								$msy = ($cnt-1);
	//							echo "total " . count($string);
								unset($string[$msy]);
								//$output = array_slice($string, 2);
//								echo "<pre>";print_r($string);
								$string1 = implode(' ', $string);
								//$string = implode(' ', $string);

							?>
                            <p><?php echo $string1;  ?>...</strong></p> <a class="meer" href="<?php the_permalink(); ?>">verder lezen ></a>

                            </div>

                            <div class="clear"></div>

                            <div class="info">

                                <ul>
                                	<li class="time"><?php the_time('l j F Y') ?></li>
                                    <li><a href="#"><?php the_author() ?></a></li>
                                    <li><a href="#"><?php comments_popup_link('0 Reacties', '1 Reacties', '% Reacties'); ?></a></li>
                                    <li class="last">

<?php $i=1; foreach((get_the_category()) as $category) {

	$category_link = get_category_link( $category->cat_ID );
	?>
   <a href="<?php echo $category_link; ?>"><?php if($i==1) { echo '  ' ; } else { echo '  , ' ; }  ?><?php echo $category->cat_name; ?></a>
   <?php $i++; }?>
    </li>
                                </ul>

                                <a href="<?php the_permalink(); ?>" class="btn">1</a>

                            </div>

                        </div>
       <?php endwhile; ?>
     <div class="clear"></div>
   <?php if(function_exists('wp_paginate')) {
    wp_paginate();
} ?>

    <?php else : ?>

    <h2 class="center">Not Found</h2>

    <p class="center">Sorry, but you are looking for something that isn't here.</p>

	<?php get_search_form(); ?>

    <?php endif; ?>




                    </div>
	                <!--  \ wire box / -->

                </div>
				<!--  \ left container / -->

                <!--  / right container \ -->
                <div id="rightCntr">

   <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Right SideBar') ) : ?> <?php endif; ?>

                </div>
                <!--  \ right container / -->

<?php get_footer(); ?>

